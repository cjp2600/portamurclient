package ru.cjp2600.portamurclient.model;

public class CommentItem {

    private String image;
    private String name;
    private String date;
    private String text;
    private String cittxt;


    public CommentItem() {
    }

    public CommentItem(String image,String name,String date,String text,String cittxt) {
        this.name = name;
        this.image = image;
        this.date = date;
        this.text = text;
        this.cittxt = cittxt;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCittxt() {
        return cittxt;
    }

    public void setCittxt(String cittxt) {
        this.cittxt = cittxt;
    }
}
