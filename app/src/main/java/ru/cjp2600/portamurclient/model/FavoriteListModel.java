package ru.cjp2600.portamurclient.model;

/**
 * Created by cjp2600 on 15.07.2014.
 */
public class FavoriteListModel {

    private String id;
    private String name;
    private String src;
    private String link;
    private String subtitle;
    private String date;

    public FavoriteListModel (String id,String name, String src, String link,String subtitle, String date ){
        this.id   = id;
        this.name = name;
        this.src  = src;
        this.link = link;
        this.subtitle = subtitle;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
