package ru.cjp2600.portamurclient.model;

public class NewsItem {

	private String title;
    private String subTitle;
    private String imageUrl;
    private String sDate;
    private String sLink;

	public NewsItem(){}

	public NewsItem(String title, String subTitle,String imageUrl,String sDate, String sLink){
		this.title    = title;
		this.subTitle = subTitle;
        this.imageUrl = imageUrl;
        this.sDate    = sDate;
        this.sLink    = sLink;
	}

	
	public String getTitle(){
		return this.title;
	}

	public void setTitle(String title){
		this.title = title;
	}

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getsDate() {
        return sDate;
    }

    public void setsDate(String sDate) {
        this.sDate = sDate;
    }

    public String getsLink() {
        return sLink;
    }

    public void setsLink(String sLink) {
        this.sLink = sLink;
    }
}
