package ru.cjp2600.portamurclient.model;

import java.io.Serializable;

public class DetailImgItem implements Serializable {

	private String src;

	public DetailImgItem(){}

	public DetailImgItem(String src){
		this.src    = src;
	}


    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }
}
