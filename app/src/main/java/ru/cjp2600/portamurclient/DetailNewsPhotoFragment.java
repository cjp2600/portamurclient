package ru.cjp2600.portamurclient;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

import ru.cjp2600.portamurclient.adapter.GridImageAdapter;
import ru.cjp2600.portamurclient.model.DetailImgItem;

public class DetailNewsPhotoFragment extends Fragment {

    private String sDetailUrl;
    private String sDetailTitle;
    private ArrayList<DetailImgItem> ImgItems;
    GridView gridView;

    public DetailNewsPhotoFragment(String sDetailUrl, String sDetailTitle,ArrayList<DetailImgItem> ImgItems)
    {
        this.sDetailTitle = sDetailTitle;
        this.sDetailUrl = sDetailUrl;
        this.ImgItems = ImgItems;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_photo, container, false);

        gridView = (GridView) rootView.findViewById(R.id.photo);
        gridView.setAdapter(new GridImageAdapter(getActivity(), ImgItems));


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {

                Bundle b = new Bundle();
                Intent intent = new Intent(getActivity(), PhotoViewActivity.class);

                b.putInt("pagerPosition",position);
                b.putSerializable("ImgItems", ImgItems);
                intent.putExtras(b);

                startActivity(intent);

            }
        });

        return rootView;
    }
}