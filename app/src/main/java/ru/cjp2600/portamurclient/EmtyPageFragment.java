package ru.cjp2600.portamurclient;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import ru.cjp2600.portamurclient.adapter.CommentListAdapter;
import ru.cjp2600.portamurclient.model.CommentItem;

public class EmtyPageFragment extends Fragment {


    AQuery aq;

    public EmtyPageFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_empty, container, false);

        return rootView;
    }
}