package ru.cjp2600.portamurclient;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import ru.cjp2600.portamurclient.helper.CLoaderView;
import ru.cjp2600.portamurclient.helper.CTextStyle;

@SuppressLint("ValidFragment")
public class DetailNewsContFragment extends Fragment {

    private String sDetailUrl;
    private String sDetailTitle;
    AQuery aq;
    private String sDetailContent;
    private String sDetailDate;
    private String nImg;
    private NewsDeatil ND;

    public DetailNewsContFragment(String sDetailUrl,String sDetailTitle,String sDetailContent,String sDetailDate,String nImg)
    {
        this.sDetailTitle = sDetailTitle;
        this.sDetailUrl = sDetailUrl;
        this.sDetailContent = sDetailContent;
        this.sDetailDate = sDetailDate;
        this.nImg = nImg;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_news_deatil, container, false);

        aq = new AQuery(rootView);

        // Устанавливаем контент.
        aq.id(R.id.newsContent).text(this.sDetailContent)
                .textSize(CTextStyle.getTextSize(getActivity()))
                .typeface(CTextStyle.getTextFont(getActivity()));

        //Log.i("FIONTS",CTextStyle.getTextFont(getActivity()) );


        aq.id(R.id.newsTitle).text(this.sDetailTitle);



        aq.id(R.id.nData).text(this.sDetailDate);
        aq.id(R.id.prevImg).image(this.nImg, true, true, 900, R.drawable.ic_launcher, null, 0, 0);

       // Toast.makeText(getActivity(), this.nImg, Toast.LENGTH_SHORT).show();

        return rootView;
    }


    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                android.support.v4.app.FragmentManager fm= getActivity().getSupportFragmentManager();
                if(fm.getBackStackEntryCount()>0){
                    fm.popBackStack();
                }
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}