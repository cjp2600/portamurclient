package ru.cjp2600.portamurclient.adapter;

import ru.cjp2600.portamurclient.DetailNewsCommentFragment;
import ru.cjp2600.portamurclient.DetailNewsContFragment;
import ru.cjp2600.portamurclient.DetailNewsPhotoFragment;
import ru.cjp2600.portamurclient.EmtyPageFragment;
import ru.cjp2600.portamurclient.model.CommentItem;
import ru.cjp2600.portamurclient.model.DetailImgItem;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class TabsPagerAdapter extends FragmentPagerAdapter {

    private String sDetailUrl;
    private String sDetailTitle;
    private String sDetailContent;
    private String sDetailDate;
    private ArrayList<DetailImgItem> ImgItems;
    private String nImg;
    private ArrayList<CommentItem> commentItems;
    private String[] tabs;
    String pagenFlag;

    public TabsPagerAdapter(FragmentManager fm,
                            String sDetailUrl,
                            String sDetailTitle,
                            String sDetailContent,
                            String sDetailDate,
                            ArrayList<DetailImgItem> ImgItems,
                            String nImg,
                            ArrayList<CommentItem> commentItems, String[] tabs, String pagenFlag) {
        super(fm);
        this.sDetailTitle = sDetailTitle;
        this.sDetailUrl = sDetailUrl;
        this.sDetailContent = sDetailContent;
        this.sDetailDate = sDetailDate;
        this.ImgItems = ImgItems;
        this.nImg = nImg;
        this.commentItems = commentItems;
        this.tabs = tabs;
        this.pagenFlag = pagenFlag;
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new DetailNewsContFragment(this.sDetailUrl,this.sDetailTitle,this.sDetailContent,this.sDetailDate,this.nImg);
            case 1:
                if (this.ImgItems.size() > 0) {
                    return new DetailNewsPhotoFragment(this.sDetailUrl, this.sDetailTitle, this.ImgItems);
                } else {
                    if (this.commentItems.size() > 0) {
                        return new DetailNewsCommentFragment(this.sDetailUrl, this.sDetailTitle, this.commentItems, this.pagenFlag);
                    } else {
                        return new EmtyPageFragment();
                    }
                }
            case 2:
                if (this.commentItems.size() > 0) {
                    return new DetailNewsCommentFragment(this.sDetailUrl, this.sDetailTitle, this.commentItems,this.pagenFlag);
                } else {
                    return new EmtyPageFragment();
                }

        }

        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return this.tabs.length;
    }

}