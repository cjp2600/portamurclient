package ru.cjp2600.portamurclient.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ru.cjp2600.portamurclient.R;
import ru.cjp2600.portamurclient.helper.DBFavorites;
import ru.cjp2600.portamurclient.model.NewsItem;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

public class NewsListAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private Context context;
    private ArrayList<NewsItem> NewsItem;
    private AQuery alist;


    public NewsListAdapter(Context context, ArrayList<NewsItem> NewsItems) {
        this.context = context;
        this.NewsItem = NewsItems;
        alist = new AQuery(context);
    }

    @Override
    public int getCount() {
        return NewsItem.size();
    }

    @Override
    public Object getItem(int position) {
        return NewsItem.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.news_row_list, null);
        }

        AQuery aq = alist.recycle(convertView);

        ImageOptions options = new ImageOptions();
        options.round = 3;
        options.animation = AQuery.FADE_IN;

        aq.id(R.id.cname).text(NewsItem.get(position).getTitle());
        aq.id(R.id.cimage).image(NewsItem.get(position).getImageUrl(),options);
        aq.id(R.id.subtitle).text(NewsItem.get(position).getSubTitle());
        aq.id(R.id.sdate).text(NewsItem.get(position).getsDate());

        DBFavorites dbf = new DBFavorites(context);
        if ( dbf.isSet(NewsItem.get(position).getsLink()) ) {
           // aq.id(R.id.imageViewStar).visibility(1);
            aq.id(R.id.itembg).getView().setBackgroundColor(context.getResources().getColor(R.color.fav));

        } else {
            aq.id(R.id.imageViewStar).visibility(View.GONE);
            aq.id(R.id.itembg).getView().setBackgroundColor(context.getResources().getColor(R.color.sowhite));
        }


        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.lheader, null);
        }
        AQuery aq = alist.recycle(convertView);

        //set header text as first char in name
        String nText = NewsItem.get(position).getsDate();
        String headerText = "";
        if (IsCurrentDate(nText,0)){
            headerText = "Сегодня";
        } else {
            if (IsCurrentDate(nText,-1)){
                headerText = "Вчера";
            } else {
                headerText = getStringDate(nText);
            }
        }

        aq.id(R.id.textHeader).text(headerText);
        aq.id(R.id.textHeader2).text(getDayOfWeek(nText));
        return convertView;
    }

    @Override
    public long getHeaderId(int i) {
        String data  = NewsItem.get(i).getsDate();
        int hashCode = data.hashCode();
        return hashCode;
    }

    public String getStringDate(String NewDate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(NewDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(convertedDate);

        Date date = cal.getTime();
        SimpleDateFormat formatDay = new SimpleDateFormat("dd");
        SimpleDateFormat formatMonth = new SimpleDateFormat("MM");
        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");

        String day   = formatDay.format(date);
        String month = formatMonth.format(date);
        String year  = formatYear.format(date);

        SimpleDateFormat Subformat = new SimpleDateFormat("dd.MM.yyyy");
        String FubFormat = Subformat.format(date);
        String mothString = "";

        if (month.equals("01")) {
            mothString = "января";
        } else if (month.equals("02")) {
            mothString = "февраля";
        } else if (month.equals("03")) {
            mothString = "марта";
        } else if (month.equals("04")) {
            mothString = "апреля";
        } else if (month.equals("05")) {
            mothString = "мая";
        } else if (month.equals("06")) {
            mothString = "июня";
        } else if (month.equals("07")) {
            mothString = "июля";
        } else if (month.equals("08")) {
            mothString = "августа";
        } else if (month.equals("09")) {
            mothString = "сентября";
        } else if (month.equals("10")) {
            mothString = "октября";
        } else if (month.equals("11")) {
            mothString = "ноября";
        } else if (month.equals("12")) {
            mothString = "декабря";
        }
        int Cyear = Calendar.getInstance().get(Calendar.YEAR);
        String Cy = String.valueOf(Cyear);

        if (year.equals(Cy)) {
            return day + " " + mothString;
        } else {
            return day + " " + mothString + " " + year;
        }
    }

    /**
     * Получение дня недели
     * @param NewDate
     * @return
     */
    public String getDayOfWeek(String NewDate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(NewDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(convertedDate);
        int daow = cal.get(Calendar.DAY_OF_WEEK);
        switch (daow) {
            case Calendar.SUNDAY:
                return "воскресенье";
            case Calendar.MONDAY:
                return "понедельник";
            case Calendar.TUESDAY:
                return "вторник";
            case Calendar.WEDNESDAY:
                return "среда";
            case Calendar.THURSDAY:
                return "четверг";
            case Calendar.FRIDAY:
                return "пятница";
            case Calendar.SATURDAY:
                return "суббота";
        }
        return "";
    }

    public boolean IsCurrentDate(String NewDate, int dayInt)
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, dayInt);

        Date date = cal.getTime();
        SimpleDateFormat formatDay = new SimpleDateFormat("dd");
        SimpleDateFormat formatMonth = new SimpleDateFormat("MM");
        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");

        String day   = formatDay.format(date);
        String month = formatMonth.format(date);
        String year  = formatYear.format(date);

        SimpleDateFormat Subformat = new SimpleDateFormat("dd.MM.yyyy");
        String FubFormat = Subformat.format(date);

        if (NewDate.equals(FubFormat)){
            return true;
        }
        return false;
    }


    class HeaderViewHolder {
        TextView text;
    }

}
