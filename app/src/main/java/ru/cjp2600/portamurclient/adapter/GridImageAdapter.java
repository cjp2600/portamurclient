package ru.cjp2600.portamurclient.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.androidquery.AQuery;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import ru.cjp2600.portamurclient.R;
import ru.cjp2600.portamurclient.model.DetailImgItem;

public class GridImageAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<DetailImgItem> ImgItems;
    private AQuery alist;


    public GridImageAdapter(Context context, ArrayList<DetailImgItem> ImgItems) {
        this.context = context;
        this.ImgItems = ImgItems;
        alist = new AQuery(context);
    }

    @Override
    public int getCount() {
        return ImgItems.size();
    }

    @Override
    public Object getItem(int position) {
        return ImgItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SquaredImageView view = (SquaredImageView) convertView;
        if (view == null) {
            view = new SquaredImageView(context);
        }

        String url = ImgItems.get(position).getSrc();
        Picasso.with(context)
                .load(url)
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .fit().centerCrop()
                .into(view);

        return view;
    }

    final class SquaredImageView extends ImageView {
        public SquaredImageView(Context context) {
            super(context);
        }

        public SquaredImageView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
        }

    }

}
