package ru.cjp2600.portamurclient.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.BitmapAjaxCallback;
import com.androidquery.callback.ImageOptions;
import com.androidquery.util.Constants;

import java.util.ArrayList;

import ru.cjp2600.portamurclient.R;
import ru.cjp2600.portamurclient.model.CommentItem;
import ru.cjp2600.portamurclient.model.NewsItem;

public class CommentListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<CommentItem> commentItems;
    private AQuery alist;


    public CommentListAdapter(Context context, ArrayList<CommentItem> commentItems) {
        this.context = context;
        this.commentItems = commentItems;
        alist = new AQuery(context);
    }

    @Override
    public int getCount() {
        return commentItems.size();
    }

    @Override
    public Object getItem(int position) {
        return commentItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.comment_row_list, null);
        }

        AQuery aq = alist.recycle(convertView);

        ImageOptions options = new ImageOptions();
        options.round = 3;

        aq.id(R.id.cit).visibility(Constants.GONE);

        String cit = commentItems.get(position).getCittxt();
        if (!cit.equals("tnocomment") && cit.length() > 0){
            aq.id(R.id.cit).visibility(1);
            aq.id(R.id.cit).margin(0, 0, 0, 5);
            aq.id(R.id.cit).text(cit.trim());
            aq.id(R.id.ctext).margin(0, 0, 0, 20);
            chLayoutParams(aq.id(R.id.cit).getTextView(),R.id.cname);
            chLayoutParams(aq.id(R.id.ctext).getTextView(), R.id.cit);
        }

       // Log.i("CIT", String.valueOf(cit.length()));
       // Log.i("CITTXT", cit);

        if (cit.equals("tnocomment"))
        {
            chLayoutParams(aq.id(R.id.ctext).getTextView(),R.id.cname);
            aq.id(R.id.ctext).margin(0, 0, 0, 10);

        }

        aq.id(R.id.cname).text(commentItems.get(position).getName().trim());
        aq.id(R.id.cimage).image(commentItems.get(position).getImage(), true, true, 50, 0);
        aq.id(R.id.ctext).text(commentItems.get(position).getText().trim());
        aq.id(R.id.cdate).text(commentItems.get(position).getDate().trim());


        return convertView;
    }

    public void chLayoutParams(TextView tv, int id){
        RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        p.addRule(RelativeLayout.BELOW, id);
        p.addRule(RelativeLayout.ALIGN_LEFT,id);
        p.addRule(RelativeLayout.ALIGN_START,id);
        tv.setLayoutParams(p);
    }

}
