package ru.cjp2600.portamurclient.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;

import java.util.ArrayList;

import ru.cjp2600.portamurclient.R;
import ru.cjp2600.portamurclient.model.FavoriteListModel;

public class FavorListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<FavoriteListModel> arFavList;
    private AQuery alist;


    public FavorListAdapter(Context context, ArrayList<FavoriteListModel> arFavList) {
        this.context   = context;
        this.arFavList = arFavList;
        alist = new AQuery(context);
    }

    @Override
    public int getCount() {
        return arFavList.size();
    }

    @Override
    public Object getItem(int position) {
        return arFavList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.news_row_list, null);
        }

        AQuery aq = alist.recycle(convertView);

        ImageOptions options = new ImageOptions();
        options.round = 3;

        aq.id(R.id.cname).text(arFavList.get(position).getName());
        aq.id(R.id.cimage).image(arFavList.get(position).getSrc(),options);
        aq.id(R.id.subtitle).text(arFavList.get(position).getSubtitle());
        aq.id(R.id.sdate).text(arFavList.get(position).getDate());

        return convertView;
    }

}
