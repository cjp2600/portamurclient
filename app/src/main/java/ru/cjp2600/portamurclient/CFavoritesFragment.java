package ru.cjp2600.portamurclient;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import ru.cjp2600.portamurclient.adapter.FavorListAdapter;
import ru.cjp2600.portamurclient.helper.DBFavorites;
import ru.cjp2600.portamurclient.model.FavoriteListModel;

public class CFavoritesFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static String ATitle;
    private FavorListAdapter adapter;
    private ArrayList<FavoriteListModel> arList;
    ListView mFavList;
    String[] navMenuTitles;
    private ArrayList<FavoriteListModel> arFavList;
    private FavorListAdapter Myadapter;

    public static CFavoritesFragment newInstance(int sectionNumber)
    {
        CFavoritesFragment fragment = new CFavoritesFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public boolean CFavoritesFragment()
    {
        return false;
    }
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_bilboard, container, false);
        // SetTitle
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        setATitle(navMenuTitles[getArguments().getInt(ARG_SECTION_NUMBER) -1]);
        mFavList = (ListView) rootView.findViewById(R.id.FavoriteList);
        DBFavorites dbf = new DBFavorites(getActivity());
        arFavList = dbf.getFavoritesList();
        Myadapter = new FavorListAdapter(getActivity(), arFavList);
        registerForContextMenu(mFavList);
        mFavList.setAdapter(Myadapter);
        Cursor cnt = dbf.selectRecords();
        int count  = cnt.getCount();

        mFavList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                Intent intent = new Intent(getActivity(), NewsDeatil.class);
                Bundle b = new Bundle();
                b.putString("detail_url", arFavList.get(position).getLink());
                b.putString("detail_title", arFavList.get(position).getName());
                intent.putExtras(b);
                startActivity(intent);
            }
        });
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_fav_actbar, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {

            case R.id.clearAllfavorite:

                new AlertDialog.Builder(getActivity())
                        .setMessage(R.string.is_clear_fav)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                DBFavorites dbf = new DBFavorites(getActivity());
                                dbf.clearAll();

                                arFavList.clear();
                                Myadapter.notifyDataSetChanged();

                            }

                        })
                        .setNegativeButton(R.string.no, null)
                        .show();

                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public MenuInflater getMenuInflater() {
        return new MenuInflater(getActivity());
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        Vibrator vs = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        vs.vibrate(500);

            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_fav, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int index = info.position;

        switch(item.getItemId()) {
            case R.id.delinfav:
                // Удаляем из избранного

                DBFavorites dbf = new DBFavorites(getActivity());
                String link = arFavList.get(index).getLink();
                dbf.deleteRecord(link);
                arFavList.remove(index);

                Myadapter.notifyDataSetChanged();

                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER),"Избранное");
    }


    public static String getATitle() {
        return ATitle;
    }

    public void setATitle(String ATitle) {
        this.ATitle = ATitle;
    }
}
