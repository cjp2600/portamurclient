package ru.cjp2600.portamurclient.helper;


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceManager;

public class CTextStyle {

    private static String sDefaultSize = "15";
    private static String sDefaultFont = "DEFAULT";
    private static SharedPreferences sp;


    public static int getTextSize(Context ctx)
    {
        try {
            sp = PreferenceManager.getDefaultSharedPreferences(ctx);
            return Integer.parseInt(sp.getString("prefSyncFrequency", sDefaultSize));
        } catch(NumberFormatException nfe) {
            return 15;
        }
    }

    public static Typeface getTextFont(Context ctx)
    {

        sp = PreferenceManager.getDefaultSharedPreferences(ctx);
        String font = sp.getString("prefSyncFrequencyFont", sDefaultFont);

        if (font.equals("DEFAULT")) {
            return Typeface.DEFAULT;

        } else if (font.equals("MONOSPACE")) {
            return Typeface.MONOSPACE;

        } else if (font.equals("SANS_SERIF")) {
            return Typeface.MONOSPACE;

        } else if (font.equals("SERIF")) {
            return Typeface.SERIF;

        }

        return null;
    }


}
