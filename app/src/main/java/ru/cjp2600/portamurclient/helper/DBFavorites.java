package ru.cjp2600.portamurclient.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Random;

import ru.cjp2600.portamurclient.model.FavoriteListModel;

public class DBFavorites {

    private CDBHelper dbHelper;
    private SQLiteDatabase database;

    public final static String EMP_TABLE = "favorites";
    public final static String EMP_ID    = "_id";
    public final static String EMP_NAME  = "name";
    public final static String EMP_SRC   = "src";
    public final static String EMP_LINK  = "link";
    public final static String EMP_SUBTITLE = "subtitle";
    public final static String EMP_DATE = "date";

    /**
     * Конструктор
     * @param context
     */
    public DBFavorites(Context context){
        dbHelper = new CDBHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    /**
     * Создание новой записи.
     * @param id
     * @param name
     * @param src
     * @param link
     * @return
     */
    public long createRecords(String id, String name, String src, String link,String subtitle,String date){
        ContentValues values = new ContentValues();
        Random rn = new Random();
        int rangetp = 20000;

        //values.put(EMP_ID, rn.nextInt(rangetp));
        values.put(EMP_NAME, name);
        values.put(EMP_SRC, src);
        values.put(EMP_LINK, link);
        values.put(EMP_SUBTITLE, subtitle);
        values.put(EMP_DATE, date);
        return database.insert(EMP_TABLE, null, values);
    }

    /**
     * Удаление
     * @param link
     * @return
     */
    public long deleteRecord(String link)
    {
        return database.delete(EMP_TABLE,EMP_LINK+" = "+"'"+link+"'",null);
    }

    /**
     * Отчистуа таблицы
     */
    public void clearAll()
    {
        database.execSQL("delete from "+ EMP_TABLE);
    }

    /**
     * Пример получения общей записи.
     * @return
     */
    public Cursor selectRecords()
    {
        String[] cols = new String[] {EMP_ID, EMP_NAME, EMP_SRC, EMP_LINK, EMP_SUBTITLE,EMP_DATE};
        Cursor mCursor = database.query(true, EMP_TABLE, cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    /**
     * Проверка на Избранное
     * @param link
     * @return
     */
    public boolean isSet (String link)
    {
        String[] cols = new String[] {EMP_LINK,};
        Cursor mCursor = database.query(true, EMP_TABLE, cols, EMP_LINK+" = "+"'"+link+"'", null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        int count = mCursor.getCount();
        if ((count > 0) ? true : false) {
            return true;

        } else {
            return false;
        }
    }

    /**
     * Получение общего списка избранного.
     * @return
     */
    public ArrayList<FavoriteListModel> getFavoritesList(){
        ArrayList<FavoriteListModel> arFavorites = new ArrayList<FavoriteListModel>();
        String[] cols = new String[] {EMP_ID, EMP_NAME, EMP_SRC, EMP_LINK,EMP_SUBTITLE,EMP_DATE};
        Cursor mCursor = database.query(true, EMP_TABLE, cols, null
                , null, null, null, EMP_ID + " DESC", null);
        if (mCursor != null) {
            mCursor.moveToFirst();

            while (!mCursor.isAfterLast()) {
                String id   = mCursor.getString(mCursor.getColumnIndex("_id"));
                String name = mCursor.getString(mCursor.getColumnIndex("name"));
                String src  = mCursor.getString(mCursor.getColumnIndex("src"));
                String link = mCursor.getString(mCursor.getColumnIndex("link"));
                String subtitle = mCursor.getString(mCursor.getColumnIndex("subtitle"));
                String date = mCursor.getString(mCursor.getColumnIndex("date"));

                arFavorites.add(new FavoriteListModel(id, name, src, link, subtitle, date));
                mCursor.moveToNext();
            }
            mCursor.close();

        }
        return arFavorites;
    }



}