package ru.cjp2600.portamurclient.helper;

import android.app.Activity;
import android.view.View;


import com.androidquery.AQuery;

import ru.cjp2600.portamurclient.R;

public class CLoaderView {
		
	public static void show(Activity act){
		AQuery aq = new AQuery(act);		
		aq.id(R.id.ContentContainer).visibility(View.GONE);
		aq.id(R.id.Loader).visibility(1);
	}
	
	public static void show(Activity act,int conent){
		AQuery aq = new AQuery(act);		
		aq.id(conent).visibility(View.GONE);
		aq.id(R.id.Loader).visibility(1);
	}
	
	public static void show(Activity act,int conent,int loader){
		AQuery aq = new AQuery(act);		
		aq.id(conent).visibility(View.GONE);
		aq.id(loader).visibility(1);
	}
	
	public static void hide(Activity act) {
		AQuery aq = new AQuery(act);	
        aq.id(R.id.Loader).visibility(View.GONE);
        aq.id(R.id.ContentContainer).visibility(1);
	}
	
	public static void hide(Activity act,int conent) {
		AQuery aq = new AQuery(act);	
        aq.id(R.id.Loader).visibility(View.GONE);
        aq.id(conent).visibility(1);
	}
	
	public static void hide(Activity act,int conent,int loader) {
		AQuery aq = new AQuery(act);	
        aq.id(loader).visibility(View.GONE);
        aq.id(conent).visibility(1);
	}
	
	
}
