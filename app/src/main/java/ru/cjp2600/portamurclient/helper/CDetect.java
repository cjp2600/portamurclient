package ru.cjp2600.portamurclient.helper;

import android.util.Log;

import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CDetect {

    public static String correctEncoding(byte[] data, String target, AjaxStatus status){

        String result = null;

        try{
            if(!"utf-8".equalsIgnoreCase(target)){
                return new String(data, target);
            }

            String header = parseCharset(status.getHeader("Content-Type"));

            Log.i("CHAR", header);

            AQUtility.debug("parsing header", header);
            if(header != null){
                return new String(data, header);
            }

            result = new String(data, "utf-8");

            String charset = getCharset(result);

            AQUtility.debug("parsing needed", charset);

            if(charset != null && !"utf-8".equalsIgnoreCase(charset)){
                AQUtility.debug("correction needed", charset);
                result = new String(data, charset);
                //status.data(result.getBytes("utf-8"));
            }

        }catch(Exception e){
            AQUtility.report(e);
        }

        return result;

    }

    public static String parseCharset(String tag){

        if(tag == null) return null;
        int i = tag.indexOf("charset");
        if(i == -1) return null;

        int e = tag.indexOf(";", i) ;
        if(e == -1) e = tag.length();

        String charset = tag.substring(i + 7, e).replaceAll("[^\\w-]", "");
        return charset;
    }

    public static String getCharset(String html){

        String pattern = "<meta [^>]*http-equiv[^>]*\"Content-Type\"[^>]*>";

        Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(html);

        if(!m.find()) return null;

        String tag = m.group();

        return parseCharset(tag);
    }

}
