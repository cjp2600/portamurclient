package ru.cjp2600.portamurclient;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.Constants;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

import ru.cjp2600.portamurclient.adapter.CommentListAdapter;
import ru.cjp2600.portamurclient.adapter.TabsPagerAdapter;
import ru.cjp2600.portamurclient.helper.CLoaderView;
import ru.cjp2600.portamurclient.model.CommentItem;
import ru.cjp2600.portamurclient.model.DetailImgItem;

@SuppressLint("ValidFragment")
public class DetailNewsCommentFragment extends Fragment {

    private String sDetailUrl;
    private String sDetailTitle;
    private ArrayList<CommentItem> commentItems;
    AQuery aq;
    ListView lv;
    View Footerview;
    AQuery fvq;
    CommentListAdapter adapter;
    int page = 1;
    String pagenFlag;
    String loadMore = "yes";


    public DetailNewsCommentFragment(String sDetailUrl, String sDetailTitle, ArrayList<CommentItem> commentItems, String pagenFlag)
    {
        this.sDetailTitle = sDetailTitle;
        this.sDetailUrl   = sDetailUrl;
        this.commentItems = commentItems;
        this.pagenFlag = pagenFlag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_comment, container, false);

        aq = new AQuery(rootView);

        lv = (ListView) rootView.findViewById(R.id.ClistView);

        // Adding button to listview at footer
        if (lv.getFooterViewsCount() > 0)
        {
            lv.removeFooterView(Footerview);
        }

        adapter = new CommentListAdapter(getActivity(), commentItems);


        if (pagenFlag.equals("yes") && loadMore.equals("yes")) {

            Footerview = inflater.inflate(R.layout.list_load_more_row, null);
            Button btnLoadMore = (Button) Footerview.findViewById(R.id.buttonMore);
            lv.addFooterView(Footerview);
            lv.setAdapter(adapter);


            /**
             * Listening to Load More button click event
             * */
            btnLoadMore.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View arg0) {
                    // Starting a new async task
                    vLoadMoreItems();
                }
            });

            lv.setOnScrollListener(new AbsListView.OnScrollListener() {
                private int visibleThreshold = 1;
                // The current offset index of data you have loaded
                private int currentPage = 0;
                // The total number of items in the dataset after the last load
                private int previousTotalItemCount = 0;
                // True if we are still waiting for the last set of data to load.
                private boolean loading = true;
                // Sets the starting page index
                private int startingPageIndex = 0;

                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    // If the total item count is zero and the previous isn't, assume the
                    // list is invalidated and should be reset back to initial state
                    // If there are no items in the list, assume that initial items are
                    // loading
                    if (!loading && (totalItemCount < previousTotalItemCount)) {
                        this.currentPage = this.startingPageIndex;
                        this.previousTotalItemCount = totalItemCount;
                        if (totalItemCount == 0) {
                            this.loading = true;
                        }
                    }

                    // If it’s still loading, we check to see if the dataset count has
                    // changed, if so we conclude it has finished loading and update the
                    // current page
                    // number and total item count.
                    if (loading) {
                        if (totalItemCount > previousTotalItemCount) {
                            loading = false;
                            previousTotalItemCount = totalItemCount;
                            currentPage++;
                        }
                    }

                    // If it isn’t currently loading, we check to see if we have breached
                    // the visibleThreshold and need to reload more data.
                    // If we do need to reload some more data, we execute onLoadMore to
                    // fetch the data.
                    if (!loading
                            && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                       // Log.i("Info", "LOADING MORE");
                        vLoadMoreItems();
                        loading = true;
                    }
                }

                public void onScrollStateChanged(AbsListView view, int scrollState) {

                }

            });
        } else {
            lv.setAdapter(adapter);
        }

        return rootView;
    }

    public void vLoadMoreItems()
    {

        if (loadMore.equals("no")){
            lv.removeFooterView(Footerview);
            return;
        }

        page += 1;

        String lu = sDetailUrl+"?PAGEN_3="+page+"&PAGEN_2="+page+"&PAGEN_1="+page;


        fvq = new AQuery(Footerview);
        fvq.id(R.id.Loader).visibility(1);
        fvq.id(R.id.ContentContainer).visibility(Constants.GONE);

        long expire = 60 * 60 * 1000;
        AjaxCallback<String> cb = new AjaxCallback<String>();
        cb.url(lu).type(String.class).weakHandler(this, "DataResponseDetail").fileCache(true).expire(expire).encoding("Cp1251");

        cb.async(getActivity());
        cb.header("Referer", "http://code.google.com/p/android-query/");
        cb.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0.2) Gecko/20100101 Firefox/6.0.2");
        aq.ajax(cb);
    }


    public void DataResponseDetail(String url, String html, AjaxStatus status){
        fetcherMore fm = new fetcherMore(html);
        fm.execute();
    }

    class fetcherMore extends AsyncTask<Void,Void, Void> {

        private Element content = null;
        private String html;
        private Element center;
        private Elements coments;
        private Elements imgs;

        public fetcherMore(String html) {
            this.html = html;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            Document doc = null;
            try {
                doc = Jsoup.parse(html);
                content = doc.getElementById("news-detail");
                center  = doc.getElementById("center");
                coments = center.select("div.reviews-block-container div.reviews-block-outer div.reviews-block-inner div.comment-item");
                imgs = content.select("div.text img");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void result)
        {
            //Название
            if (sDetailTitle != null) {
                sDetailTitle = content.select("div.meta h1").text();
            }



                Element Pobj = center.select("div.reviews-navigation-box div.reviews-page-navigation div.modern-page-navigation a.modern-page-next").first();

                if (Pobj == null){
                    loadMore = "no";
                    Log.i("INFOOOO","NO");
                } else {
                    Log.i("INFOOOO","YES");
                }


            for (Element comment : coments) {

                // аватар
                String image  = comment.select("div.img img").attr("src");
                if (image.length() > 2) {
                    String imageF = String.valueOf(image.charAt(0)) + String.valueOf(image.charAt(1));
                    if (imageF.equals("//")) {
                        image = "http:" + image;
                    } else {
                        image = "http://portamur.ru" + image;
                    }
                } else {
                    image = "http://portamur.ru/bitrix/templates/portamur_2013/components/portamur/news/template1/spichka/news.detail/.default/images/img.png";
                }

                // Имя
                String name = comment.select("div.inner div.post div.meta span.name").text();
                String date = comment.select("div.inner div.post div.meta span.date").text();

                //Цитата
                String text = "";
                String cittxt = "tnocomment";
                Element cit = comment.select("table.forum-quote").first();
                if (cit != null){

                    cittxt = comment.select("table.forum-quote tbody tr td").text();
                    cit.remove();

                    text = comment.select("div.inner div.post div").last().text();

                   // Log.i("CITATA",cittxt);
                } else {
                    text = comment.select("div.inner div.post div").last().text();

                }

                commentItems.add(new CommentItem(image,name,date,text,cittxt));
                adapter.notifyDataSetChanged();
            }


        }

    }

}