package ru.cjp2600.portamurclient;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ShareActionProvider;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.util.ArrayList;

import ru.cjp2600.portamurclient.adapter.TabsPagerAdapter;
import ru.cjp2600.portamurclient.helper.CLoaderView;
import ru.cjp2600.portamurclient.model.CommentItem;
import ru.cjp2600.portamurclient.model.DetailImgItem;

public class NewsDeatil extends FragmentActivity implements
        ActionBar.OnNavigationListener  {

    private String sDetailUrl = "";
    private String sDetailTitle = "";
    AQuery aq;
    private ShareActionProvider mShareActionProvider;

    private ViewPager viewPager;
    private TabsPagerAdapter mAdapter;
    private ActionBar actionBar;
    private ArrayList<DetailImgItem> ImgItems;
    private ArrayList<CommentItem> CommentItems;


    private String sDetailContent;

    // Tab titles
    private String[] tabs = { "Содержание", "Фото", "Комментарии" };

    public NewsDeatil() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        aq = new AQuery(this);
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(actionBar.getThemedContext(),
                android.R.layout.simple_spinner_item, android.R.id.text1,
                tabs);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set up the dropdown list navigation in the action bar.
        actionBar.setListNavigationCallbacks(adapter, new  NewsDeatil());

        if(getIntent().getExtras()!=null){
            Bundle b   = getIntent().getExtras();
            sDetailUrl = b.getString("detail_url");
            sDetailTitle = b.getString("detail_title");

            if ( getIntent().getData()!=null)
            {
                Uri data = getIntent().getData();
                sDetailUrl = data.toString();
                sDetailTitle = "";
            }
        }

        if (sDetailUrl.length()>0)
        {
            String last = String.valueOf(sDetailUrl.charAt(sDetailUrl.length() -1 ));
            if (!last.equals("/")) {
                sDetailUrl = sDetailUrl+"/";
            }
        }

        // Получаем Информацию
        CLoaderView.show(this);
        long expire = 60 * 60 * 1000;
        AjaxCallback<String> cb = new AjaxCallback<String>();
        cb.url(sDetailUrl).type(String.class).weakHandler(this, "getCallbackAjaxDetail").fileCache(true).expire(expire).encoding("Cp1251");

        cb.async(this);
        cb.header("Referer", "http://code.google.com/p/android-query/");
        cb.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0.2) Gecko/20100101 Firefox/6.0.2");
        aq.ajax(cb);

        viewPager = (ViewPager) findViewById(R.id.pager);
    }

    @Override
    public boolean onNavigationItemSelected(int i, long l) {
        Log.i("INFO_I",String.valueOf(i));
        return true;
    }


    class AsyncParseDetailPage extends AsyncTask<Void,Void, Void> implements
            ActionBar.OnNavigationListener {

        private Element  content = null;
        private String html;
        private Element center;
        private Elements coments;
        private Elements imgs;
        private Element DopImages;

        public AsyncParseDetailPage(String html) {
            this.html = html;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            Document doc = null;
            try {
                doc = Jsoup.parse(html);
                content = doc.getElementById("news-detail");
                center  = doc.getElementById("center");
                coments = center.select("div.reviews-block-container div.reviews-block-outer div.reviews-block-inner div.comment-item");
                imgs = content.select("div.text img");
                DopImages = center.getElementById("photogallery-news-detail");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }
        @Override
        protected void onPostExecute(Void result)
        {

            String titleNew = content.select("div.meta h1").text();
            getActionBar().setSubtitle(titleNew);
            //Название
            if (sDetailTitle != null) {
                sDetailTitle = titleNew;
            }
            // Конент
            String nContent = content.select("div.text").text();
            // Дата
            String nData = content.select("div.meta div.data div.date").text();
            String nImg  = content.select("div.img img").attr("src");
            if (nImg.length() > 2) {
                String first = String.valueOf(nImg.charAt(0)) + String.valueOf(nImg.charAt(1));
                if (first.equals("//")) {
                    nImg = "http:" + nImg;
                } else {
                    nImg = "http://portamur.ru" + nImg;
                }
            } else {
                nImg = "http://portamur.ru/bitrix/templates/portamur_2013/components/portamur/news/template1/spichka/news.detail/.default/images/img.png";
            }


            // Комментарии
            CommentItems = new ArrayList<CommentItem>();
            for (Element comment : coments) {

                // аватар
                String image  = comment.select("div.img img").attr("src");
                if (image.length() > 2) {
                    String imageF = String.valueOf(image.charAt(0)) + String.valueOf(image.charAt(1));
                    if (imageF.equals("//")) {
                        image = "http:" + image;
                    } else {
                        image = "http://portamur.ru" + image;
                    }
                } else {
                    image = "http://portamur.ru/bitrix/templates/portamur_2013/components/portamur/news/template1/spichka/news.detail/.default/images/img.png";
                }

                // Имя
                String name = comment.select("div.inner div.post div.meta span.name").text();
                String date = comment.select("div.inner div.post div.meta span.date").text();

                //Цитата
                String text = "";
                String cittxt = "tnocomment";
                Element cit = comment.select("table.forum-quote").first();
                if (cit != null){

                    cittxt = comment.select("table.forum-quote tbody tr td").text();
                    cit.remove();

                    Element attach = comment.select("div.inner div.post div.reviews-message-img").first();
                    if (attach == null) {
                        text = comment.select("div.inner div.post div").eq(2).text();
                    } else {
                        text = comment.select("div.inner div.post div").eq(1).text();
                    }

                   // Log.i("CITATA",cittxt);
                } else {
                    Element attach = comment.select("div.inner div.post div.reviews-message-img").first();
                    if (attach == null) {
                        text = comment.select("div.inner div.post div").eq(2).text();
                    } else {
                        text = comment.select("div.inner div.post div").eq(1).text();
                    }

                }

                text = text.trim();

                CommentItems.add(new CommentItem(image,name,date,text,cittxt));
            }

            String pagenFlag = "nopagen";
            Element Pobj = center.select("div.modern-page-navigation").first();
            if (Pobj != null){
                pagenFlag = "yes";
            }

            // Изображения
            ImgItems = new ArrayList<DetailImgItem>();
            //  ImgItems.add(new DetailImgItem(nImg));

            if (DopImages != null){

                Element contImg = DopImages.select("div.photo-items-list").first();
                Elements arImgs = contImg.select("div");

                for (Element Dimg : arImgs){
                    String src = Dimg.select("a.photo-item-inner").attr("href");

                    Log.i("SUB",src);


                    if (src.length() > 4) {
                        String first = String.valueOf(src.charAt(0)) + String.valueOf(src.charAt(1)) + String.valueOf(src.charAt(2)) + String.valueOf(src.charAt(3));
                        if (!first.equals("http")) {

                            if (first.equals("//po")){
                                src = "http:" + src;

                                Log.i("New Image",src);

                            } else {

                                /**
                                 * Этот костыль для того чтоб picasso загржал
                                 * фото с русскими именами + пробелами
                                 */
                                src = "http://portamur.ru" + src;
                                URI uri = null;
                                int pos = src.lastIndexOf('/') + 1;
                                try {
                                    src = src.replace(" ", "_________");
                                    uri = new URI(src.substring(0, pos) + URLEncoder.encode(src.substring(pos), "utf-8"));
                                    uri = new URI(uri.toString().replace("_________", "%20"));

                                } catch (URISyntaxException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                src = uri.toString();

                            }

                        }
                    } else {
                        src = "http://portamur.ru/bitrix/templates/portamur_2013/components/portamur/news/template1/spichka/news.detail/.default/images/img.png";
                    }
                    ImgItems.add(new DetailImgItem(src));
                }

                if (ImgItems.size() > 1){
                    ImgItems.remove(0);
                }

            } else {


                for (Element img : imgs) {
                    String src = img.attr("src");
                    if (src.length() > 4) {
                        String first = String.valueOf(src.charAt(0)) + String.valueOf(src.charAt(1)) + String.valueOf(src.charAt(2)) + String.valueOf(src.charAt(3));
                        if (!first.equals("http")) {

                            if (first.equals("//po")){
                                src = "http:" + src;

                                Log.i("New Image",src);

                            } else {

                                /**
                                 * Этот костыль для того чтоб picasso загржал
                                 * фото с русскими именами + пробелами
                                 */
                                src = "http://portamur.ru" + src;
                                URI uri = null;
                                int pos = src.lastIndexOf('/') + 1;
                                try {
                                    src = src.replace(" ", "_________");
                                    uri = new URI(src.substring(0, pos) + URLEncoder.encode(src.substring(pos), "utf-8"));
                                    Log.i("SUB", uri.toString());
                                    uri = new URI(uri.toString().replace("_________", "%20"));

                                } catch (URISyntaxException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                src = uri.toString();
                            }

                        }
                    } else {
                        src = "http://portamur.ru/bitrix/templates/portamur_2013/components/portamur/news/template1/spichka/news.detail/.default/images/img.png";
                    }
                    ImgItems.add(new DetailImgItem(src));
                }


            }


            if (ImgItems.size() > 0) {
                tabs[1] = "Фото (" + String.valueOf(ImgItems.size()) + ")";
                if (CommentItems.size() > 0){
                    String plus = (pagenFlag.equals("yes")) ? "+":"";
                    tabs[2] = "Комментарии (" + String.valueOf(CommentItems.size()) + ""+plus+")";
                }
            }

            if (ImgItems.size() == 0)
            {
                tabs = new String[]{tabs[0], tabs[2]};
                if (CommentItems.size() > 0){
                    String plus = (pagenFlag.equals("yes")) ? "+":"";
                    tabs[1] = "Комментарии (" + String.valueOf(CommentItems.size()) + ""+plus+")";
                }
            }

            // Initilization

            actionBar = getActionBar();
            mAdapter = new TabsPagerAdapter(getSupportFragmentManager(), sDetailUrl,sDetailTitle,nContent,nData,ImgItems,nImg,CommentItems,tabs,pagenFlag);

            viewPager.setAdapter(mAdapter);



            // Specify a SpinnerAdapter to populate the dropdown list.
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(actionBar.getThemedContext(),
                    android.R.layout.simple_spinner_item, android.R.id.text1,
                    tabs);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // Set up the dropdown list navigation in the action bar.
            actionBar.setListNavigationCallbacks(adapter, this);


            /**
             * on swiping the viewpager make respective tab selected
             * */
            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    // on changing the page
                    // make respected tab selected
                    actionBar.setSelectedNavigationItem(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            });

            CLoaderView.hide(NewsDeatil.this);
            // Hide the list
        }

        @Override
        public boolean onNavigationItemSelected(int i, long l) {
            viewPager.setCurrentItem(i);
            return true;
        }
    }



    public void getCallbackAjaxDetail(String url, String html, AjaxStatus status)
    {
        if (html.length() > 0) {
            AsyncParseDetailPage fm = new AsyncParseDetailPage(html);
            fm.execute();
        } else {
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.news_deatil, menu);

        MenuItem item = menu.findItem(R.id.menu_item_share);
        ShareActionProvider myShareActionProvider = (ShareActionProvider) item.getActionProvider();

        String shareText = "Порт Амур: "+sDetailTitle+ " "+ sDetailUrl;
        Intent myIntent = new Intent();
        myIntent.setAction(Intent.ACTION_SEND);
        myIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        myIntent.setType("text/plain");
        myShareActionProvider.setShareIntent(myIntent);


        return true;
    }

    // Call to update the share intent
    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            finish();
           // overridePendingTransition(0, 0);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                // Toast.makeText(getApplication(), "Back", Toast.LENGTH_LONG).show();
                this.finish();
                // overridePendingTransition(0, 0);
                break;


            default:
                break;
        }
        return true;
    }

    public void Fin(){
        this.finish();
    }


    public void setsDetailContent(String sDetailContent) {
        this.sDetailContent = sDetailContent;
    }
}
