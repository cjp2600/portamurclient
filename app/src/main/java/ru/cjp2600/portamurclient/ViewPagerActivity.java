package ru.cjp2600.portamurclient;

import ru.cjp2600.portamurclient.model.DetailImgItem;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;

import android.view.Menu;
import android.view.MenuItem;

import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ShareActionProvider;
import android.widget.Toast;


import com.androidquery.AQuery;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class ViewPagerActivity extends Activity {

    private static final int SEND_REQUEST = 1;
    private ViewPager mViewPager;
    private MenuItem menuLockItem;
    private ArrayList<DetailImgItem> ImgItems;
    private AQuery aq;
    private int pagerPosition;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        //getActionBar().hide();
        getActionBar().setDisplayHomeAsUpEnabled(true);
        //getWindow().setWindowAnimations(0);
        setContentView(R.layout.view_pager);

        Bundle b   = getIntent().getExtras();
        pagerPosition = b.getInt("pagerPosition");

        ImgItems = (ArrayList<DetailImgItem>) getIntent().getSerializableExtra("ImgItems");

        mViewPager =  (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(new ImagePagerAdapter(ImgItems,this,pagerPosition));
        mViewPager.setCurrentItem(pagerPosition);

        getActionBar().setTitle(String.valueOf(mViewPager.getCurrentItem()+1)+" из "+String.valueOf(ImgItems.size()));

        aq = new AQuery(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.view_pager_menu, menu);

        return true;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            finish();
            overridePendingTransition(0, 0);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

     //   Log.i("INFO","CKIKKKK");

        switch (item.getItemId()) {
            case android.R.id.home:
               // Toast.makeText(getApplication(), "Back", Toast.LENGTH_LONG).show();
                this.finish();
               // overridePendingTransition(0, 0);
                break;



            default:
                break;
        }
        return super.onOptionsItemSelected(item);

    }


    private class ImagePagerAdapter extends PagerAdapter {

        private final Context ctx;
        private ArrayList<DetailImgItem> ImgItems;
        private LayoutInflater inflater;
        private int pagerPosition;

        ImagePagerAdapter(ArrayList<DetailImgItem> ImgItems, Context ctx, int pagerPosition) {
            this.ImgItems = ImgItems;
            inflater = getLayoutInflater();
            this.ctx = ctx;
            this.pagerPosition = pagerPosition;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }

        @Override
        public void finishUpdate(View container) {
        }

        @Override
        public int getCount() {
            return ImgItems.size();
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {

            final int INT_POS = position;

            View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
            AQuery aq = new AQuery(imageLayout);
            getActionBar().setTitle(String.valueOf(position)+" из "+String.valueOf(ImgItems.size()));


            //Toast.makeText(ImagePagerActivity.this, message, Toast.LENGTH_SHORT).show();

           // WebView wv = (WebView) imageLayout.findViewById(R.id.web);
            //wv.getSettings().setUseWideViewPort(true);

            //WebSettings webSettings = wv.getSettings();
            //webSettings.setBuiltInZoomControls(true);

           // final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
           // spinner.setVisibility(View.VISIBLE);
           // aq.id(R.id.image).progress(R.id.loading).image(ImgItems.get(position).getSrc());

            aq.id(R.id.web).progress(R.id.progress).webImage(ImgItems.get(position).getSrc(), true, true, 0xFF000000);


               // spinner.setVisibility(View.GONE);
            ImageButton mybutton = (ImageButton) imageLayout.findViewById(R.id.shBtn);
            mybutton.setOnClickListener(new View.OnClickListener() {

                public static final int SEND_REQUEST = 1;

                @Override
                public void onClick(View v) {
                    AQuery aq = new AQuery(v);

                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.DAY_OF_MONTH, 0);

                    Date date = cal.getTime();
                    SimpleDateFormat Subformat = new SimpleDateFormat("dd_MM_yyyy");
                    String FubFormat = Subformat.format(date);

                    String url = ImgItems.get(INT_POS).getSrc();
                    File file = aq.makeSharedFile(url, FubFormat+"_news_portamur.jpg");

                    if(file != null){
                        Intent intent = new Intent(Intent.ACTION_SEND);
                        intent.setType("image/jpeg");
                        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
                        startActivityForResult(Intent.createChooser(intent, "Что использовать?"), SEND_REQUEST);
                    }

                }
            });


            ((ViewPager) view).addView(imageLayout, 0);
            return imageLayout;
        }



        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }

        @Override
        public void startUpdate(View container) {
        }
    }


}
