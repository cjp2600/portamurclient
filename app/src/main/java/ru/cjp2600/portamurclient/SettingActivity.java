package ru.cjp2600.portamurclient;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.text.Html;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.util.AQUtility;

import java.io.File;
import java.text.DecimalFormat;

import static ru.cjp2600.portamurclient.R.string.aboutText;

public class SettingActivity extends PreferenceFragment implements
        SharedPreferences.OnSharedPreferenceChangeListener {

    AQuery aq;
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static String ATitle;

    public static SettingActivity newInstance(int sectionNumber) {
        SettingActivity fragment = new SettingActivity();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        addPreferencesFromResource(R.xml.pref);
        setATitle("Настройки");

        aq = new AQuery(getActivity());

        long size = 0;
        File cacheDirectory = getActivity().getCacheDir();
        File[] files = cacheDirectory.listFiles();
        for (File f:files) {
            size = size+f.length();
        }

        Preference cache_clean = (Preference) getPreferenceScreen().findPreference("key_cleancache");
        cache_clean.setSummary("Всего кэша: " + readableFileSize(size));
        cache_clean.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                // TODO Отчичтьить кэш

                try {
                    trimCache(getActivity());

                    Toast.makeText(getActivity(), "Кэш приложния удален", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }



                return true;
            }
        });


        Preference lk = (Preference) getPreferenceScreen().findPreference("key_lkenter");
        lk.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {

                Toast.makeText(getActivity(), "Взод", Toast.LENGTH_SHORT).show();

              return true;
            }
        });


        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;

        Preference dialogPreference = (Preference) getPreferenceScreen().findPreference("dialog_preference");
        dialogPreference.setSummary("версия "+ version);
        dialogPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                // dialog code here
                showAbout();
                return true;
            }
        });

    }

    public static String readableFileSize(long size) {
        if(size <= 0) return "0";
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    protected void showAbout() {
        // Inflate the about message contents
        View messageView = getActivity().getLayoutInflater().inflate(R.layout.about, null, false);

        AQuery q = aq.recycle(messageView);

        // When linking text, force to always use default color. This works
        // around a pressed color state bug.

        q.id(R.id.aImge).image("http://portamur.ru/upload/medialibrary/40d/logo.jpg");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
       // builder.setTitle(getString(R.string.app_name) + " версия 1.0");
        builder.setView(messageView);
        builder.create();
        builder.show();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
        if (key.equals("key_cleancache")) {
            long size = 0;
            File cacheDirectory = getActivity().getCacheDir();
            File[] files = cacheDirectory.listFiles();
            for (File f : files) {
                size = size + f.length();
            }
            Preference pc = (Preference) getPreferenceScreen().findPreference(key);
            pc.setSummary("Всего кэша!: " + readableFileSize(size));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister the listener whenever a key changes
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER),"Афиша");
    }


    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return true;
    }


    public static String getATitle() {
        return ATitle;
    }

    public void setATitle(String ATitle) {
        this.ATitle = ATitle;
    }


}
