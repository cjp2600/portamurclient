package ru.cjp2600.portamurclient;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PMnenieFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static String ATitle;
    String[] navMenuTitles;

    public static PMnenieFragment newInstance(int sectionNumber) {
        PMnenieFragment fragment = new PMnenieFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    public PMnenieFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_pmnenie, container, false);

        // SetTitle
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        setATitle(navMenuTitles[getArguments().getInt(ARG_SECTION_NUMBER) -1]);
         
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER),"Афиша");
    }


    public static String getATitle() {
        return ATitle;
    }

    public void setATitle(String ATitle) {
        this.ATitle = ATitle;
    }
}
