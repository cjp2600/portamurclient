package ru.cjp2600.portamurclient;


import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ErrorNetworkFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    public static ErrorNetworkFragment newInstance(int sectionNumber) {
        ErrorNetworkFragment fragment = new ErrorNetworkFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    public ErrorNetworkFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_errornetwork, container, false);
         
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER),"Ошибка");
    }
}
