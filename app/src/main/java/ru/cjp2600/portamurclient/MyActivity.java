package ru.cjp2600.portamurclient;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.support.v4.widget.DrawerLayout;
import com.androidquery.AQuery;


public class MyActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private AQuery aq;
    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private String[] navMenuTitles;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        mTitle = getTitle();

        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        aq = new AQuery(this);
    }

    /**
     * Проверка на полключение к Internet
     * @return
     */
    private boolean isNetworkConnected()
    {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            new AlertDialog.Builder(this)
                    .setMessage(R.string.really_quit)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            MyActivity.this.finish();
                        }
                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
            return true;
        }
        else {
            return super.onKeyDown(keyCode, event);
        }
    }


    @Override
    public void onNavigationDrawerItemSelected(int position)
    {
        displayView(position);
    }

    public void onSectionAttached(int number,String Atitle)
    {
        if (number == 404) {
            mTitle = getString(R.string.app_name);
        } else {
            mTitle = Atitle;
        }
    }

    public  void restoreActionBar() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    private void displayView(int position)
    {
        if (!isNetworkConnected()){
            position = 404;
        }
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = NewsFragment.newInstance(position + 1);
                break;
            case 1:
                fragment = CFavoritesFragment.newInstance(position + 1);
                break;
            case 2:
                fragment = SettingActivity.newInstance(position + 1);
                //fragment = new CommunityFragment();
                break;
            case 3:
                fragment = PMnenieFragment.newInstance(position + 1);
                break;
            case 4:
                //fragment = new PagesFragment();
                break;
            case 5:

                break;
            case 404:
                fragment = ErrorNetworkFragment.newInstance(position);
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment).commit();
        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
        if(isTaskRoot()){
           // AQUtility.cleanCacheAsync(this);
        }
    }
}
