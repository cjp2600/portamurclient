package ru.cjp2600.portamurclient;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.Constants;
import com.squareup.timessquare.CalendarPickerView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import ru.cjp2600.portamurclient.adapter.NewsListAdapter;
import ru.cjp2600.portamurclient.helper.CLoaderView;
import ru.cjp2600.portamurclient.helper.DBFavorites;
import ru.cjp2600.portamurclient.helper.PtrStickyListHeadersListView;
import ru.cjp2600.portamurclient.model.NewsItem;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

public class NewsFragment extends Fragment  implements
        ActionBar.OnNavigationListener {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
    private static String ATitle;

    private AQuery aq;
    private NewsListAdapter adapter;
    private ArrayList<NewsItem> NewsItems;
    private PtrStickyListHeadersListView mNewsListView;
    private String day;
    private String month;
    private String year;
    int current_page;
    AQuery fvq;
    int SELECTED = 0;
    View Footerview;
    String dataNews;
    private PullToRefreshLayout mPullToRefreshLayout;
    String ghost;
    View rootView;
    private static final String TAG = "SampleTimesSquareActivity";
    private CalendarPickerView calendar;
    private AlertDialog theDialog;
    private CalendarPickerView dialogView;

    public static NewsFragment newInstance(int sectionNumber)
    {
        NewsFragment fragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

	public NewsFragment(){}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_news, container, false);
        aq = new AQuery(getActivity(),rootView);


        setATitle(getResources().getString(R.string.new_blg));
        //getActivity().getActionBar().setTitle(ATitle);

        return rootView;
    }


    @Override
    public void onActivityCreated (Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        final ActionBar actionBar = getActivity().getActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        final String[] dropdownValues = getResources().getStringArray(R.array.action_list);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(actionBar.getThemedContext(),
                android.R.layout.simple_spinner_item, android.R.id.text1,
                dropdownValues);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        actionBar.setListNavigationCallbacks(adapter, this);

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.newsfragment_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {

            case R.id.menu_calendar:

                // Календарь
                final Calendar nextYear = Calendar.getInstance();
                nextYear.add(Calendar.DAY_OF_MONTH, 1);


                final Calendar lastYear = Calendar.getInstance();
                lastYear.add(Calendar.YEAR, -1);

                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

                dialogView = (CalendarPickerView) inflater.inflate(R.layout.calendar_dialog, null, false);
                dialogView.init(lastYear.getTime(), nextYear.getTime()) //
                        .withSelectedDate(new Date());
                theDialog =
                        new AlertDialog.Builder(getActivity())
                                .setView(dialogView)
                                .setNeutralButton("Выбрать", new DialogInterface.OnClickListener() {
                                    @Override public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();

                                        long selectTime = dialogView.getSelectedDate().getTime();

                                        SimpleDateFormat formatDay    = new SimpleDateFormat("dd");
                                        SimpleDateFormat formatMonth  = new SimpleDateFormat("MM");
                                        SimpleDateFormat formatYear   = new SimpleDateFormat("yyyy");

                                        String fday   = formatDay.format(selectTime);
                                        String fmonth = formatMonth.format(selectTime);
                                        String fyear  = formatYear.format(selectTime);

                                        SimpleDateFormat Subformat = new SimpleDateFormat("dd.MM.yyyy");
                                        String FubFormat = Subformat.format(selectTime);

                                        CLoaderView.show(getActivity());
                                        calendarNews(fday,fmonth,fyear,ghost);

//                                        Toast.makeText(getActivity(), String.valueOf(iiday) , Toast.LENGTH_SHORT).show();

                                    }
                                })
                                .create();
                theDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        Log.d(TAG, "onShow: fix the dimens!");
                        dialogView.fixDialogDimens();

                    }
                });
                theDialog.show();


                break;

            default:
                break;
        }

        getActivity().getActionBar().setTitle(this.getATitle());
        return super.onOptionsItemSelected(item);
    }

    public MenuInflater getMenuInflater() {
        return new MenuInflater(getActivity());
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_list, menu);

            Vibrator vs = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            vs.vibrate(500);

            // Проверка на добавленое в избранное
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            int position = info.position;
            DBFavorites dbf = new DBFavorites(getActivity());

            if ( dbf.isSet(NewsItems.get(position).getsLink()) ) {
                menu.findItem(R.id.addtofavor).setVisible(false);
                menu.findItem(R.id.delinfav).setVisible(true);
            }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int index = info.position;

        switch(item.getItemId()) {
            case R.id.addtofavor:
               // Добавляем в избранное

                DBFavorites dbf = new DBFavorites(getActivity());

                if ( !dbf.isSet(NewsItems.get(index).getsLink()) ) {

                    // Добавляем в избранное
                    dbf.createRecords(
                            "1",
                            NewsItems.get(index).getTitle(),
                            NewsItems.get(index).getImageUrl(),
                            NewsItems.get(index).getsLink(),
                            NewsItems.get(index).getSubTitle(),
                            NewsItems.get(index).getsDate()
                    );
                    adapter.notifyDataSetChanged();

                 //   Toast.makeText(getActivity(), "Новость добавлена в избранное.", Toast.LENGTH_SHORT).show();
                } else {

                  //  Toast.makeText(getActivity(), "Новость уже есть в избранном.", Toast.LENGTH_SHORT).show();

                }

                return true;

            case R.id.delinfav:
                // Удаляем из Избранного
                DBFavorites obDel = new DBFavorites(getActivity());

                if ( obDel.isSet(NewsItems.get(index).getsLink()) ) {
                    obDel.deleteRecord(NewsItems.get(index).getsLink());
                    adapter.notifyDataSetChanged();

                  //  Toast.makeText(getActivity(), "Новость удалена из избранного", Toast.LENGTH_SHORT).show();
                }

                return true;

            case R.id.openinbrouser:
                // Открываем в браузере

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(NewsItems.get(index).getsLink()));
                startActivity(i);
                getActivity().overridePendingTransition(0, 0);

                return true;

            case R.id.copylink:
                // Копируем ссылку

                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

                ClipboardManager clipboard;
                clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("link", NewsItems.get(info.position).getsLink());
                clipboard.setPrimaryClip(clip);

                Toast.makeText(getActivity(), getActivity().getString(R.string.link_is_copy) , Toast.LENGTH_SHORT).show();

                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    public void calendarNews(String day,String month,String year, String host)
    {

        Calendar thatDay = Calendar.getInstance();
        thatDay.set(Calendar.DAY_OF_MONTH,Integer.parseInt(day));
        thatDay.set(Calendar.MONTH,Integer.parseInt(month) - 1); // 0-11 so 1 less
        thatDay.set(Calendar.YEAR, Integer.parseInt(year));

        Calendar today = Calendar.getInstance();
        long diff = today.getTimeInMillis() - thatDay.getTimeInMillis();
        long days = diff / (24 * 60 * 60 * 1000);

        current_page = (int) -days;

        String url = host+"?day="+day+"&month="+month+"&year="+year;
        long expire = 30 * 60 * 1000;

        dataNews = day+"."+month+"."+year;

        AjaxCallback<String> cb = new AjaxCallback<String>();
        cb.url(url).type(String.class).weakHandler(this, "DataResponseCalendar").fileCache(true).expire(expire).encoding("Cp1251");

        cb.header("Referer", "http://code.google.com/p/android-query/");
        cb.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0.2) Gecko/20100101 Firefox/6.0.2");
        aq.ajax(cb);
    }

    public void setNews(int number,String host)
    {
        CLoaderView.show(getActivity());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, number);
        ghost = host;

        Date date = cal.getTime();
        SimpleDateFormat formatDay = new SimpleDateFormat("dd");
        SimpleDateFormat formatMonth = new SimpleDateFormat("MM");
        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");

        day   = formatDay.format(date);
        month = formatMonth.format(date);
        year  = formatYear.format(date);

        SimpleDateFormat Subformat = new SimpleDateFormat("dd.MM.yyyy");
        String FubFormat = Subformat.format(date);

        String url = host+"?day="+day+"&month="+month+"&year="+year;
        long expire = 30 * 60 * 1000;

        dataNews = day+"."+month+"."+year;

        AjaxCallback<String> cb = new AjaxCallback<String>();
        cb.url(url).type(String.class).weakHandler(this, "DataResponse").fileCache(true).expire(expire).encoding("Cp1251");

        cb.header("Referer", "http://code.google.com/p/android-query/");
        cb.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0.2) Gecko/20100101 Firefox/6.0.2");
        aq.ajax(cb);
    }

    class fetcher extends AsyncTask<Void,Void, Void> implements
            OnRefreshListener {

        private Element  content = null;
        private Elements links = null;
        private String   html;

        public fetcher(String html) {
            this.html = html;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            Document doc = null;
            try {
                doc = Jsoup.parse(html);
                content = doc.getElementById("news");
                links = content.getElementsByClass("news_item");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if ( links != null ) {
                for (Element link : links) {

                    String sNewsName = link.select("div.news_item_body div.news_item_name a").text();
                    String sNewsImg = link.select("a div.img").attr("style");
                    sNewsImg = sNewsImg.replace("background: url('", "");
                    sNewsImg = sNewsImg.replace("') 0 0 no-repeat;", "");
                    if (sNewsImg.length() > 2) {
                        char fc = sNewsImg.charAt(0);
                        char ft = sNewsImg.charAt(1);

                        String first = String.valueOf(fc) + String.valueOf(ft);

                        if (first.equals("//")) {
                            sNewsImg = "http:" + sNewsImg;
                        } else {
                            sNewsImg = "http://portamur.ru" + sNewsImg;
                        }
                    } else {
                        sNewsImg = "http://portamur.ru/bitrix/templates/portamur_2013/components/portamur/news/template1/spichka/news.detail/.default/images/img.png";
                    }

                    String sData  = link.select("div.news_item_body div.news_item_head").text();
                    sData = dataNews; //sData.trim();
                    String sAnons = link.select("div.news_item_body div.news_item_prev").text();
                    String sLink  = link.select("div.news_item_body div.news_item_name a").attr("href");
                    sLink = "http://portamur.ru" + sLink;
                    NewsItems.add(new NewsItem(sNewsName, sAnons, sNewsImg, sData, sLink));
                }

                adapter = new NewsListAdapter(getActivity(), NewsItems);
                mNewsListView = (PtrStickyListHeadersListView) getActivity().findViewById(R.id.NewsList);

                mPullToRefreshLayout = (PullToRefreshLayout) rootView.findViewById(R.id.ptrlayoutnews);
                ActionBarPullToRefresh.from(getActivity())
                        .allChildrenArePullable()
                        .useViewDelegate(PtrStickyListHeadersListView.class, mNewsListView)
                        .listener(this)
                        .setup(mPullToRefreshLayout);

                LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

                if (mNewsListView.getFooterViewsCount() > 0) {
                    mNewsListView.removeFooterView(Footerview);
                }

                Footerview = inflater.inflate(R.layout.list_load_more_row, null);
                Button btnLoadMore = (Button) Footerview.findViewById(R.id.buttonMore);
                mNewsListView.addFooterView(Footerview);

                mNewsListView.setAdapter((se.emilsjolander.stickylistheaders.StickyListHeadersAdapter) adapter);

                registerForContextMenu(mNewsListView);

                btnLoadMore.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        vLoadMoreItems();
                    }
                });

                mNewsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                        Intent intent = new Intent(getActivity(), NewsDeatil.class);
                        Bundle b = new Bundle();
                        b.putString("detail_url", NewsItems.get(position).getsLink());
                        b.putString("detail_title", NewsItems.get(position).getTitle());
                        intent.putExtras(b);
                        startActivity(intent);

                    }
                });

                mNewsListView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    private int visibleThreshold = 1;
                    private int currentPage = 0;
                    private int previousTotalItemCount = 0;
                    private boolean loading = true;
                    private int startingPageIndex = 0;

                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        if (!loading && (totalItemCount < previousTotalItemCount)) {
                            this.currentPage = this.startingPageIndex;
                            this.previousTotalItemCount = totalItemCount;
                            if (totalItemCount == 0) {
                                this.loading = true;
                            }
                        }

                        if (loading) {
                            if (totalItemCount > previousTotalItemCount) {
                                loading = false;
                                previousTotalItemCount = totalItemCount;
                                currentPage++;
                            }
                        }

                        if (!loading
                                && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                            Log.i("Info", "LOADING MORE");
                            vLoadMoreItems();
                            loading = true;
                        }
                    }
                    public void onScrollStateChanged(AbsListView view, int scrollState) {
                    }

                });

                // Если Нет новостей за сегодня то
                // Дополнительно грузим за вчера.

                if (NewsItems.size() == 0) {
                    vLoadMoreItems();
                }

                CLoaderView.hide(getActivity());
                mPullToRefreshLayout.setRefreshComplete();

            } else {

                // Если нет Элементов.
                Toast.makeText(getActivity(), "Ошибка получения данных" , Toast.LENGTH_SHORT).show();
                mPullToRefreshLayout.setRefreshComplete();

            }
        }

        @Override
        public void onRefreshStarted(View view) {
            current_page = 0;
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, 0);

            Date date = cal.getTime();
            SimpleDateFormat formatDay = new SimpleDateFormat("dd");
            SimpleDateFormat formatMonth = new SimpleDateFormat("MM");
            SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");

            day   = formatDay.format(date);
            month = formatMonth.format(date);
            year  = formatYear.format(date);

            SimpleDateFormat Subformat = new SimpleDateFormat("dd.MM.yyyy");
            String FubFormat = Subformat.format(date);

            String url = ghost+"?day="+day+"&month="+month+"&year="+year;
            long expire = -1;

            dataNews = day+"."+month+"."+year;

            AjaxCallback<String> cb = new AjaxCallback<String>();
            cb.url(url).type(String.class).weakHandler(this, "DataResponseRefresh").fileCache(true).expire(expire).encoding("Cp1251");

            cb.header("Referer", "http://code.google.com/p/android-query/");
            cb.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0.2) Gecko/20100101 Firefox/6.0.2");
            aq.ajax(cb);
        }

        public void DataResponseRefresh(String url, String html, AjaxStatus status)
        {
            NewsItems = new ArrayList<NewsItem>();
            NewsItems.clear();
            fetcher f = new fetcher(html);
            f.execute();
        }
    }



    /**
     * Обработка результата запроса
     * @param url
     * @param html
     * @param status
     */
    public void DataResponse(String url, String html, AjaxStatus status)
    {

        NewsItems = new ArrayList<NewsItem>();
        NewsItems.clear();

        fetcher f = new fetcher(html);
        f.execute();
    }

    public void DataResponseCalendar(String url, String html, AjaxStatus status)
    {

        NewsItems = new ArrayList<NewsItem>();
        NewsItems.clear();

        fetcher f = new fetcher(html);
        f.execute();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MyActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER), ATitle);
    }


    protected void vLoadMoreItems()
    {
        current_page -= 1;
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, current_page);

        Date date = cal.getTime();
        SimpleDateFormat formatDay = new SimpleDateFormat("dd");
        SimpleDateFormat formatMonth = new SimpleDateFormat("MM");
        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
        day   = formatDay.format(date);
        month = formatMonth.format(date);
        year  = formatYear.format(date);
        SimpleDateFormat Subformat = new SimpleDateFormat("dd.MM.yyyy");
        String FubFormat = Subformat.format(date);

        fvq = new AQuery(Footerview);
        fvq.id(R.id.Loader).visibility(1);
        fvq.id(R.id.ContentContainer).visibility(Constants.GONE);

        String host = "";
        switch (SELECTED){
            case 0:
                host = "http://portamur.ru/news/archive/";
                break;
            case 1:
                host = "http://portamur.ru/neighbours/archive/";
                break;
            case 2:
                host = "http://portamur.ru/jobnews/archive/";
                break;
            case 3:
                host = "hhttp://portamur.ru/travelnews/archive/";
                break;
            case 4:
                host = "http://portamur.ru/economics/newsarchive/";
                break;
            case 5:
                host = "http://portamur.ru/economics/newspartner/index.php";
                break;
            case 6:
                host = "http://portamur.ru/autonews/archive/";
                break;
        }

        String url = host+"?day="+day+"&month="+month+"&year="+year;
        long expire = 10080 * 60 * 1000;
        dataNews = day+"."+month+"."+year;

        AjaxCallback<String> cb = new AjaxCallback<String>();
        cb.url(url).type(String.class).weakHandler(this, "DataResponseLoadMore").fileCache(true).expire(expire).encoding("Cp1251");

        cb.header("Referer", "http://code.google.com/p/android-query/");
        cb.header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0.2) Gecko/20100101 Firefox/6.0.2");
        aq.ajax(cb);
    }

    /**
     * Асинхронная обработка
     */
    class fetcherMore extends AsyncTask<Void,Void, Void>
    {
        private Element  content = null;
        private Elements links = null;
        private String html;

        public fetcherMore(String html) {
            this.html = html;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            Document doc = null;
            try {
                doc = Jsoup.parse(html);
                content = doc.getElementById("news");
                links = content.getElementsByClass("news_item");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if (links.size() > 0) {

                for (Element link : links) {

                    String sNewsName = link.select("div.news_item_body div.news_item_name a").text();
                    String sNewsImg = link.select("a div.img").attr("style");
                    sNewsImg = sNewsImg.replace("background: url('", "");
                    sNewsImg = sNewsImg.replace("') 0 0 no-repeat;", "");

                    if (sNewsImg.length() > 2) {

                        char fc = sNewsImg.charAt(0);
                        char ft = sNewsImg.charAt(1);
                        String first = String.valueOf(fc) + String.valueOf(ft);

                        if (first.equals("//")) {
                            sNewsImg = "http:" + sNewsImg;
                        } else {
                            sNewsImg = "http://portamur.ru" + sNewsImg;
                        }
                    } else {
                        sNewsImg = "http://portamur.ru/bitrix/templates/portamur_2013/components/portamur/news/template1/spichka/news.detail/.default/images/img.png";
                    }

                    String sData = link.select("div.news_item_body div.news_item_head").text();
                    sData = dataNews;
                    String sAnons = link.select("div.news_item_body div.news_item_prev").text();
                    String sLink = link.select("div.news_item_body div.news_item_name a").attr("href");
                    sLink = "http://portamur.ru" + sLink;
                    NewsItems.add(new NewsItem(sNewsName, sAnons, sNewsImg, sData, sLink));
                    adapter.notifyDataSetChanged();
                }

                fvq.id(R.id.Loader).visibility(Constants.GONE);
                fvq.id(R.id.ContentContainer).visibility(1);

            } else {
                vLoadMoreItems();
            }
        }
    }

    public void DataResponseLoadMore(String url, String html, AjaxStatus status)
    {
        fetcherMore fmm = new fetcherMore(html);
        fmm.execute();
    }

    public static String getATitle() {
        return ATitle;
    }

    public void setATitle(String ATitle) {
        this.ATitle = ATitle;
    }


    @Override
    public boolean onNavigationItemSelected(int i, long l)
    {
        switch (i) {
            case 0:
                this.setATitle(getResources().getString(R.string.new_blg));
                current_page = 0;
                setNews(current_page,"http://portamur.ru/news/archive/");
                SELECTED = 0;
                break;

            case 5:
                this.setATitle(getResources().getString(R.string.new_neiborhood));
                current_page = 0;
                setNews(current_page,"http://portamur.ru/neighbours/archive/");
                SELECTED = 1;
                break;

            case 3:
                this.setATitle(getResources().getString(R.string.new_job));
                current_page = 0;
                setNews(current_page,"http://portamur.ru/jobnews/archive/");
                SELECTED = 2;
                break;

            case 1:
                this.setATitle(getResources().getString(R.string.new_econimic));
                current_page = 0;
                setNews(current_page,"http://portamur.ru/economics/newsarchive/");
                SELECTED = 4;
                break;

            case 4:
                this.setATitle(getResources().getString(R.string.new_partners));
                current_page = 0;
                setNews(current_page,"http://portamur.ru/economics/newspartner/index.php");
                SELECTED = 5;
                break;

            case 2:
                this.setATitle(getResources().getString(R.string.new_auto));
                current_page = 0;
                setNews(current_page,"http://portamur.ru/autonews/archive/");
                SELECTED = 6;
                break;

            default:
                break;
        }
        return true;
    }

}